package cl.bci.integ.core.util;

import cl.bci.integ.core.exception.IntegrationCode;
import cl.bci.integ.core.exception.IntegrationException;

public class TuxParamUtil {

	private TuxParamUtil() {
		
	}
	
	public static String setWithValidation(String paramName, String value, int maxLen) {
		
		String res = null;
		
		if (value != null) {
			if (value.length() > maxLen) {
				throw new IntegrationException(IntegrationCode.INVALID_PARAM, "Parametro '" + paramName + "' excede largo maximo: " + maxLen);
			}
			res = value;
		}
		
		return res;
	}
	
	
}
