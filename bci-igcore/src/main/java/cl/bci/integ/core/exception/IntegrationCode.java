package cl.bci.integ.core.exception;

public enum IntegrationCode {

	NO_CONNECTION,
	TIMEOUT,
	DATABASE_ERROR,
	DATABASE_CONNECTION_FAIL,
	INVALID_PARAM,
	TUXEDO_ERROR,
	GENERIC;
	
}
