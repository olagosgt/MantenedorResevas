package cl.bci.integ.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import cl.bci.integ.core.exception.IntegrationCode;
import cl.bci.integ.core.exception.IntegrationException;

/**
 * Utilitario para el manejo de fechas
 * 
 * @author fmontoya
 *
 */
public class DateUtil {

	private DateUtil() {
		
	}
	
	public static String formatDate(java.sql.Date date, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}

	public static java.sql.Date parseDate(String paramName, String strDate, String pattern) {
		java.sql.Date sqlDate = null;
		
		if (strDate != null && !strDate.isEmpty()) {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			try {
				java.util.Date date = sdf.parse(strDate);
				sqlDate = new java.sql.Date(date.getTime());
			} catch (ParseException e) {
				throw new IntegrationException(IntegrationCode.GENERIC, "Campo '" + paramName + "'. Fecha mal formada: '" + 
						strDate + "'. Formato esperado: '" + pattern + "'.", e);
			}
		}
		
		return sqlDate;
	}

	public static java.sql.Timestamp parseTimestamp(String paramName, String strDate, String pattern) {
		java.sql.Timestamp sqlTs = null;
		
		if (strDate != null && !strDate.isEmpty()) {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			try {
				java.util.Date date = sdf.parse(strDate);
				sqlTs = new java.sql.Timestamp(date.getTime());
			} catch (ParseException e) {
				throw new IntegrationException(IntegrationCode.GENERIC, "Campo '" + paramName + "'. Fecha-hora mal formada: '" + 
						strDate + "'. Formato esperado: '" + pattern + "'.", e);
			}
		}
		
		return sqlTs;
	}
	
	public static java.sql.Date newDate(int year, int month, int date) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, date, 0, 0, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return new java.sql.Date(cal.getTimeInMillis());
	}

	public static java.sql.Timestamp newTimestamp(int year, int month, int date, int hour, int minutes, int seconds) {
		Calendar cal = Calendar.getInstance();
		cal.set(year, month, date, hour, minutes, seconds);
		cal.set(Calendar.MILLISECOND, 0);
		return new java.sql.Timestamp(cal.getTimeInMillis());
	}
	
}
