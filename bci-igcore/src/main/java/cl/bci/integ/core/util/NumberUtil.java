package cl.bci.integ.core.util;

import java.math.BigDecimal;

public class NumberUtil {

	private NumberUtil() {
		
	}
	
	public static Integer parseInteger(String value) {
		
		Integer res = null;
		
		if (value != null && !value.trim().isEmpty()) {
			res = Integer.valueOf(value);
		}
		
		return res;
	}
	
	public static Long parseLong(String value) {
		
		Long res = null;
		
		if (value != null && !value.trim().isEmpty()) {
			res = Long.valueOf(value);
		}
		
		return res;
	}

	public static BigDecimal parseBigDecimal(String value) {
		
		BigDecimal res = null;
		
		if (value != null && !value.trim().isEmpty()) {
			res = new BigDecimal(value.replace(".", "").replace(",", "."));
		}
		
		return res;
	}
	
}
