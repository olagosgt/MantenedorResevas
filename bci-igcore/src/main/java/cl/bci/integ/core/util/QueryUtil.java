package cl.bci.integ.core.util;

public class QueryUtil {

	private QueryUtil() {
		
	}
	
	public static String quotes(int num) {
		
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < num; i++) {
			if (i > 0) {
				sb.append(", ");
			}
			sb.append("?");
		}
		
		return sb.toString();
	}
	
	public static void close(AutoCloseable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (Exception e) {
				// Ignored
			}
		}
	}
}
