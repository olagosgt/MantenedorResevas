package cl.bci.integ.core.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Eexception when an integration exception is produced in server side
 * 
 * @author fmontoya
 *
 */
public class IntegrationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Integration code
	 */
	private final IntegrationCode code;
	
	/**
	 * Tuxedo service name, when integration code is TUXEDO_ERROR 
	 */
	private String tuxedoServiceName;
	
	/**
	 * Tuxedo error code, when integration code is TUXEDO_ERROR
	 */
	private String tuxedoCode;

	/**
	 * Tuxedo error description, when integration code is TUXEDO_ERROR
	 */
	private String tuxedoDescription;

	/**
	 * Tuxedo additional information, when integration code is TUXEDO_ERROR
	 */
	private String tuxedoAdditionalInformation;

	/**
	 * Stack trace exception. Nested exception is not stored because can't
	 * be serialized to client.
	 */
	private final String technicalDetails;

	public IntegrationException(IntegrationCode code) {
		super(defaultMessage(code));
		this.code = code;
		this.technicalDetails = null;
	}

	public IntegrationException(IntegrationCode code, Throwable throwable) {
		super(defaultMessage(code));
		this.code = code;
		this.technicalDetails = getDetails(throwable);
	}
	
	public IntegrationException(IntegrationCode code, String message) {
		super(message);
		this.code = code;
		this.technicalDetails = null;
	}

	public IntegrationException(IntegrationCode code, String message, Throwable throwable) {
		super(message);
		this.code = code;
		this.technicalDetails = getDetails(throwable);
	}

	public IntegrationException(String tuxedoServiceName, String tuxedoCode, String tuxedoDescription, String tuxedoAdditionalInformation, Throwable throwable) {
		super();
		this.code = IntegrationCode.TUXEDO_ERROR;
		this.tuxedoServiceName = tuxedoServiceName;
		this.tuxedoCode = tuxedoCode;
		this.tuxedoDescription = tuxedoDescription;
		this.tuxedoAdditionalInformation = tuxedoAdditionalInformation;
		this.technicalDetails = getDetails(throwable);
	}
	
	public IntegrationCode getCode() {
		return code;
	}
	
	public String getTuxedoCode() {
		return tuxedoCode;
	}

	public String getTuxedoDescription() {
		return tuxedoDescription;
	}

	public String getTuxedoAdditionalInformation() {
		return tuxedoAdditionalInformation;
	}

	public String getTechnicalDetails() {
		return technicalDetails;
	}

	private static String defaultMessage(IntegrationCode code) {
		return "Integration exception. Code: " + code;
	}

	private static String getDetails(Throwable throwable) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		throwable.printStackTrace(pw);
		return sw.toString();
	}
	
}
